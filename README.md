# AG OCEAN OBS

home of mareHUB AG ocean obs [https://spaces.awi.de/x/3rTMEg](https://spaces.awi.de/x/3rTMEg)

## Mapping Tables
- following .sdi.mapping.tab files [https://spaces.awi.de/x/RhAUFg](https://spaces.awi.de/x/RhAUFg)
- in some table certain columns (such as pangaea ID, abbreviation, ...) still exist. this is intended.
- x == considered done by NA (or at least a version to work with)
- (x) == preliminary version, might be final, at least can be worked with


|   |                      | **parameters** | **units** | **methods** |
|---|----------------------|----------------|-----------|-------------|
| 1 | salinity             | x              | x         | x           |
| 1 | temperature          | x              | x         | x           |
| 1 | turbidity            | x              | x         | x           |
|   |                      |                |           |             |
| 2 | alkalinity/ph        | x              | x         | (x)         |
| 2 | chlorophyll          | x              | x         | x           |
| 2 | current              | (x)            |           | x           |
| 2 | oxygen               |                |           |             |
| 2 | pressure             |                |           |             |
| 2 | total organic carbon |                |           |             |
|   |                      |                |           |             |
| 3 | tbc                  |                |           |             |



## 5ct per parameter

### salinity
#### methods
- calculated, derived, or modelled -- obvious
- mechanical -- different coefficients of thermal expansion, strictly oldschool
- electrical -- anything related to resistance, current etc.
- by remote sensing (space and air) -- not to be confused with handheld stuff...
#### units
#### parameters


### temperature
#### methods
- mechanical -- measured by thermal expansion of agents, such as mercury thermometer or bimetal stuff
- electrical -- temperature dependent change of electrical resistance, voltage, ...
- calculated -- e.g. from proxies or by guesstimation
- by remote sensing -- space or airborne measurement of electromagnetic spectra
#### units
#### parameters

### turbidity
#### methods

modern turbimeters usually use photometric method. 
clarification needed if there is a more useful distinction possible/reasonable.<2021-11-25>

#### units

similar to pH, but still completely different 

#### parameters




### alkalinity/pH
#### methods
- calculated -- not directly measured but *somehow* derived
- measured, colorimetric or spectrometric -- anything related to color, wavelenghts, or spectra
- measured, potentiometric -- the one with electrodes
- measured, isfet -- electric conductivity at transistor -> [en.wikipedia.org/wiki/ISFET](https://en.wikipedia.org/wiki/ISFET)
#### units

pH is dimensionless. puh, unit mapping done :D

#### parameters

### chlorophyll
#### parameters

#### methods
distinction in
- direct -- direct extraction and further treatment either per spectroscopy or by fluorescence 
- indirect -- drive-by shoo... measurement via fluorescence
- count --  counting of algae stuff
#### units






### current
#### methods

- acoustic, profile
- acoustic, single-point
- inductive
- mechanic, single-point
- remote sensing
#### units
#### parameters

With regards to velocity for some pangaea datasets the current velocity might be messed up with geological lifting rates, sinking rates in lab experiments, etc. Example: pangaea IDs 105470 and 126775 can be distinguished by units only (both velocity magnitude with units `m/a` and `m/s` respectively).




















